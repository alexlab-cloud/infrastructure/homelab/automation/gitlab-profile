# Homelab Automation

This namespace contains projects related to automating the configuration of workstations and servers. Unless otherwise
noted, all machines use Ubuntu 22.04 or above.

## Minimal Homelab Configuration

Start with two (or more) machines, each with automation users and SSH (client and server) capabilities enabled.

### All Machines

#### Add an Automation User

Add a user (with a password) as a sudoer, and skip their [Gecos](https://en.wikipedia.org/wiki/Gecos_field) information:

```bash
sudo adduser "automation" --ingroup sudo --gecos ""
```

Switch to the user with `su automation`, or whichever username you used in `adduser`.

#### Create SSH Keys

As the automation user, create new SSH keys ([Ed25519](https://ed25519.cr.yp.to/) preferred) with a comment and filenames that make it obvious these are for system automation:

```bash
ssh-keygen -t ed25519 -C "${HOSTNAME}_automation" -f "${HOME}/.ssh/id_ed25519-${HOSTNAME}_automation" -P ""
```

### On the Home Machine

On the machine where most of the automation will be run (e.g. the main driver PC), switch to the automation user and  enable automated logins via SSH:

#### Copy SSH IDs Between Machines

Use [`ssh-copy-id`](https://www.ssh.com/academy/ssh/copy-id) to upload the SSH user key to the target and edit its `authorized_keys` file with information from the client host. `ssh-copy-id` uses port 22 by default.

```bash
su automation
ssh-copy-id -i "${HOME}/.ssh/id_ed25519-${HOSTNAME}_automation" "automation@target-hostname"
```

<hr>

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>

<hr>
